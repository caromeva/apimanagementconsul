package com.globant.microservices;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Caromeva on 31-05-17.
 */
@RestController
public class HelloController {

    @RequestMapping("/helloConsul")
    public String helloWorld() {
        return new String("Hello Consul Api Management Globant");
    }
}
