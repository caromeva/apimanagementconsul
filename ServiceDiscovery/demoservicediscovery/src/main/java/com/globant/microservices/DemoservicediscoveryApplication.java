package com.globant.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class DemoservicediscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoservicediscoveryApplication.class, args);
	}
}
