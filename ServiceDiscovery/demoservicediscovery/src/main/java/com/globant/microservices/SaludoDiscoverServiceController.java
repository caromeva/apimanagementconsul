package com.globant.microservices;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Caromeva on 31-05-17.
 */
@RestController
public class SaludoDiscoverServiceController {

    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    org.springframework.cloud.client.discovery.DiscoveryClient client;

    @RequestMapping(value = "/hello/saludo", method = RequestMethod.GET)
    public String saludosGlobant() {

        org.springframework.cloud.client.ServiceInstance serviceInstance =
        client.getInstances("Globant-Hacker-Api-Management")
                .stream()
                .findFirst()
                .orElseThrow(() -> new RuntimeException("city-service not found"));
        String url = serviceInstance.getUri().toString() + "/helloConsul";
        //String url = "http://localhost:8080/helloConsul";

        //return restTemplate.getForObject(url, String.class);
        return url;
    }

}
